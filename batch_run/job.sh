#!/bin/bash
unset MARLIN_DLL
source /cvmfs/cepc.ihep.ac.cn/software/cepcenv/setup.sh
cepcenv use 0.1.1
PACKAGEPATH=/cefs/higgs/baiy/mypackages/FSClasser_2020_1_21
export MARLIN_DLL=$PACKAGEPATH/lib/libFSClasser.so:$MARLIN_DLL

directory=$1
inputfile=$2
cd $directory
copypath=/cefs/higgs/baiy/mypackages/FSClasser_2020_1_21/batch_run
rm -rf ./job.xml
cp $copypath/job.xml ./
sed -i "s/zzzzzzzzzz/$inputfile/g" job.xml
#Marlin job.xml
