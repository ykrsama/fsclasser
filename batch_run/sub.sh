#!/bin/bash
inputpath=/cefs/tmp_storage/baiy/floating-jobs/simu_mumuH/*
for d in $inputpath
  do 
    for f in $d/*dst.slcio
      do
        file=`echo $f|cut -d"/" -f8`
        hep_sub job.sh -g higgs -argu $d $file
    done
done
