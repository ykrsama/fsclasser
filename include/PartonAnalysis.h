#ifndef PARTONANALYSIS_H
#define PARTONANALYSIS_H
#include "lcio.h"
#include <map>
#include <vector>
#include "FSClasserProcessor.h"
bool SortDecayChain(EVENT::MCParticle *p1, EVENT::MCParticle *p2);
map<EVENT::MCParticle*,vector<EVENT::MCParticle*> >GetPrimaryParton(vector<EVENT::MCParticle*> inputPartonList);
#endif
