/**
 * @brief Marlin processor for finding isolated leptons.
 * @author Ryo Yonamine <yonamine@post.kek.jp>
 * @author Tomohiko Tanabe <tomohiko@icepp.s.u-tokyo.ac.jp>
 * @version $Id:$
 *
 * Given a list of ReconstructedParticle, identify isolated leptons
 * based on the track cone energy, lepton identification,
 * and the track impact parameters (optional).
 */

#ifndef IsolatedLeptonFinderProcessor_h
#define IsolatedLeptonFinderProcessor_h 1

#include <string>
#include <map>

#include <marlin/Processor.h>
#include <lcio.h>

#include <EVENT/ReconstructedParticle.h>
#include <IMPL/ReconstructedParticleImpl.h>
#include <EVENT/MCParticle.h>
#include <UTIL/LCRelationNavigator.h>

using namespace lcio ;
using namespace marlin ;

class PhotonCorrectionProcessor : public Processor {
	
	public:
		virtual Processor*  newProcessor() { return new PhotonCorrectionProcessor ; }

                PhotonCorrectionProcessor() ;

                virtual void init() ;
                virtual void processRunHeader( LCRunHeader* run ) ;
                virtual void processEvent( LCEvent * evt ) ;
                virtual void check( LCEvent * evt ) ;
                virtual void end() ;

	protected:
		LCCollection* _pfoCol;
                LCCollection*_IsolepCol;
                std::string _inputPFOsCollection;
                std::string _inputIsolationLeptonCollection;
                std::string _outputPFOsRemovedIsoLepCollection;
//                std::string _outputIsoLepCollection;
		std::string _outputPhotonCollection;
                double _cosConeAngle;
                
};

#endif
