#ifndef SELFDECAYHELPER_H
#define SELFDECAYHELPER_H
#include "lcio.h"
#include <EVENT/MCParticle.h>

lcio::MCParticle * SelfDecay_RecurseDown(lcio::MCParticle * p); //Looking downward 
lcio::MCParticle * SelfDecay_RecurseUp(lcio::MCParticle * p); //Looking upward
bool OnOneBranch(lcio::MCParticle* p1, lcio::MCParticle* p2);
lcio::MCParticle *  Lepton_RecurseDown(lcio::MCParticle *p);
lcio::MCParticle *  LeptonNavigator(lcio::MCParticle *p);
bool LeptonContinue_RecurseDown(lcio::MCParticle *p);
#endif
