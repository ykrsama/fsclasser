#! /bin/bash
nfperjob=1 #keep it consistant with that in testsub.sh
version=v20_240

isub=0
while read run  #run is MC number or runnumber or MC folder name
     do
     
     PBS_O_WORKDIR=/cefs/tmp_storage/baiy/floating-jobs
     directory=/cefs/data/DstData/CEPC240/CEPC_v4_update/higgs
     logfilepath=/cefs/tmp_storage/baiy/job_log_files
     PBS_O_WORKDIR=$PBS_O_WORKDIR"/"$run"AnalyFT/"$version
     logfilepath=$logfilepath"/"$run"/"$version

     mkdir -p $logfilepath
     mkdir -p $PBS_O_WORKDIR

     counter=0

     inputsubfolder=""
     if [ "$run" = "qqH" ]
        then inputsubfolder="qqh_151112"
     fi

     if [ "$run" = "nnH" ]
       then inputsubfolder="nnh_151110"
     fi
     if [ "$run" = "eeH" ]
       then inputsubfolder="eeh"
     fi
     
     if [ "$run" = "mumuH" ]
       then inputsubfolder="CEPC_v4_240_mmh_Inclusive"
     fi
 
     if [ "$run" = "eeHbb" ]
       then inputsubfolder="E240.Pe1e1h_bb.e0.p0.whizard195"
     fi    

     if [ "$run" = "eeHcc" ]
       then inputsubfolder="E240.Pe1e1h_cc.e0.p0.whizard195"
     fi

     if [ "$run" = "eeHgg" ]
       then inputsubfolder="E240.Pe1e1h_gg.e0.p0.whizard195"
     fi
     
     if [ "$run" = "eeHww" ]
       then inputsubfolder="E240.Pe1e1h_ww.e0.p0.whizard195"
     fi

     if [ "$run" = "eeHzz" ]
       then inputsubfolder="E240.Pe1e1h_zz.e0.p0.whizard195"
     fi

     if [ "$run" = "eeHX" ]
       then inputsubfolder="E240.Pe1e1h_X.e0.p0.whizard195"
     fi     

     if [ "$run" = "tautauH" ]
       then  inputsubfolder="sig_e3e3h_2"
     fi
     for f in `find  $directory/$inputsubfolder/*.slcio*`
        do let counter=counter+1
     done

     echo $run " file number : "$counter


     if [ 0 == `expr $counter % $nfperjob`  ]
     then 
       njobs=`expr $counter / $nfperjob` #njobs=(counter/nfperjob);
     else
       njobs=`expr $counter / $nfperjob + 1` #njobs=(counter/nfperjob)+1;
     fi


     for ((j=1;j<=njobs;j++))
       do
         if [ -d $PBS_O_WORKDIR/$run.$j ]
         then echo "$run.$j already exist"
         else echo "job $run.$j will be submitted"
               hep_sub -g higgs job.sh -argu $run $j
#              if [ 1 == `expr $isub % 3` ]
#              then
#                 qsub -q hsimq -o $logfilepath -e $logfilepath -N $run.$j job_2.sh
#              elif [ 2 == `expr $isub % 3`  ]
#              then 
#                 qsub -q cepcq@pbssrv02  -o $logfilepath -e $logfilepath -N $run.$j job_2.sh
#              else
#                 qsub -q cepcq@pbssrv02 -o $logfilepath -e $logfilepath -N $run.$j job_2.sh
#              fi
              let isub=isub+1
         fi   
     done

done<input_run.txt
