#! /bin/bash
version=v22_240_1M_pdgvtx

PBS_O_WORKDIR=/cefs/tmp_storage/baiy/floating-jobs
directory=/cefs/higgs/baiy/datalink/DstData/CEPC240/CEPC_v4_update/higgs
nfperjob=1 #keep it consistant with that in testjob.sh
localpath=/cefs/higgs/baiy/mypackages/FSClasser_2020_1_21

insert_lines()
{
   filename=$1
   mark=$2
   linenew=$3 # linenew can be a 
   #  echo "file : "$filename
   #  echo "mark : "$mark
   #  echo "line : "$linenew
   grep -n "$mark" $filename | cut -d":" -f1 > line.tmp
   for n in $(cat line.tmp)
     do
       i=`echo $n | bc`
       head -n $i $filename > tmp_1
       let i=i+1
       tail -n +$i $filename > tmp_2
       #echo "$linenew" | tee -a tmp_1
       cat $linenew >> tmp_1
       cat tmp_2 >> tmp_1
       mv tmp_1 $1
       done
   rm -rf tmp_1
   rm -rf line.tmp
}

insert_line()
{
   filename=$1
   mark=$2
   linenew=$3 # linenew can be a 
   #  echo "file : "$filename
   #  echo "mark : "$mark
   #  echo "line : "$linenew
   grep -n "$mark" $filename | cut -d":" -f1 > line.tmp
   for n in $(cat line.tmp)
     do
       i=`echo $n | bc`
       head -n $i $filename > tmp_1
       let i=i+1
       tail -n +$i $filename > tmp_2
       echo "$linenew" | tee -a tmp_1
       cat tmp_2 >> tmp_1
       mv tmp_1 $1
       done
   rm -rf tmp_1
   rm -rf line.tmp
}
PBS_JOBNAME=$1.$2
run=`echo $PBS_JOBNAME|cut -d"." -f1`
index=`echo $PBS_JOBNAME|cut -d"." -f2`
PBS_O_WORKDIR=$PBS_O_WORKDIR"/"$run"AnalyFT/"$version

cd $localpath
source loadLDD_new.sh

mkdir -p $PBS_O_WORKDIR/$PBS_JOBNAME
cd $PBS_O_WORKDIR/$PBS_JOBNAME

echo $PWD

cp $localpath/run/mumuHgg/job.xml ./mumuH_Isolep.xml

inputsubfolder=""
if [ "$run" = "qqH" ]
  then inputsubfolder="qqh_151112"
fi
if [ "$run" = "nnH" ]
  then inputsubfolder="nnh_151110"
fi
if [ "$run" = "eeH" ]
  then inputsubfolder="e1e1h_151107"
fi

if [ "$run" = "mumuH" ]
  then inputsubfolder="CEPC_v4_240_mmh_Inclusive"
fi

if [ "$run" = "mumuHbb" ]
  then inputsubfolder="E240.Pe2e2h_bb.e0.p0.whizard195"
fi

if [ "$run" = "mumuHcc" ]
  then inputsubfolder="E240.Pe2e2h_cc.e0.p0.whizard195"
fi

if [ "$run" = "mumuHgg" ]
  then inputsubfolder="E240.Pe2e2h_gg.e0.p0.whizard195"
fi

if [ "$run" = "mumuHww" ]
  then inputsubfolder="E240.Pe2e2h_ww.e0.p0.whizard195"
fi

if [ "$run" = "mumuHzz" ]
  then inputsubfolder="E240.Pe2e2h_zz.e0.p0.whizard195"
fi

if [ "$run" = "mumuHX" ]
  then inputsubfolder="E240.Pe2e2h_X.e0.p0.whizard195"
fi

if [ "$run" = "tautauH" ]
  then  inputsubfolder="sig_e3e3h_2"
fi

find $directory/$inputsubfolder/*.slcio > tmp_input.txt
#find $directory/$run/$version/*.slcio > tmp_input.txt

counter=1
imax=`expr $nfperjob \* $index`
imin=`expr $imax - $nfperjob`
echo "imax = "$imax
echo "imin = "$imin

touch tmp_input_subjob.txt
for inputdir in `cat tmp_input.txt`
  do
    if [ $counter -gt $imin  ]&&[ $counter -le $imax ]
    then
      echo "        "$inputdir >> tmp_input_subjob.txt
    fi
  let counter=counter+1
done

insert_lines mumuH_Isolep.xml "LCIOInputFiles" tmp_input_subjob.txt
insert_line mumuH_Isolep.xml "name=\"DSTOutput\"\ type=\"LCIOOutputProcessor\"" "      output$index.slcio"

Marlin mumuH_Isolep.xml > mumuHlog_$index.log
