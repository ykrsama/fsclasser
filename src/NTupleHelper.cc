#include "NTupleHelper.h"
#include "Utilities.h"
#include "TMatrixD.h"
#include "TVectorD.h"
#include <iostream>
const int NTupleHelper::maxsize =  300;
// ********************************************************************
//    EVENT INFORMATION
// ********************************************************************
void
NTupleHelper::fillEvent( LCEvent * evtP){
	fillDouble("Run",        evtP->getRunNumber()   );
	fillDouble("Event",      evtP->getEventNumber() );
}

double NTupleHelper::GetTrackProb(Track * ptrk){
        TMatrixD cov(5,5);
        for(int i =0; i<5;i++){
             for(int j=0;j<=i;j++){
                  cov[i][j] = ptrk->getCovMatrix()[i*(i+1)/2+j];
             }
        }
        for(int i=0;i<4;i++){
             for(int j=i+1;j<5;j++){
                  cov[i][j] = cov[j][i];
             }
        }
    

        TMatrixD vec1(1,5), vec2(1,5);
        TMatrixD vec1T(5,1), vec2T(5,1);
/*
        vec1[0][0] = ptrk->getD0();
        vec1[0][1] = ptrk->getPhi();
        vec1[0][2] = ptrk->getOmega();
        vec1[0][3] = ptrk->getZ0();
        vec1[0][4] = ptrk->getTanLambda();
*/

        vec1[0][0] = -ptrk->getD0();
        vec1[0][1] = 0;
        vec1[0][2] = 0;
        vec1[0][3] = -ptrk->getZ0();
        vec1[0][4] = 0;


        for(int i=0; i< 5; i++){             
            vec1T[i][0] = vec1[0][i];                
        }

        cov.Invert();
/*
        std::cout<<"Mark A"<<std::endl;
        std::cout<<"Matrix X Row: "<<cov.GetNrows()<<std::endl;
        std::cout<<"Matrix X Col: "<<cov.GetNcols()<<std::endl;
        std::cout<<"Martix Y Row: "<<TMatrixD::Transpose(vec1).GetNrows()<<std::endl;
        std::cout<<"Matrix Y Col: "<<TMatrixD::Transpose(vec1).GetNcols()<<std::endl;
*/
        TMatrixD v_tmp = cov*vec1T;

/*
        std::cout<<"Matrix A Row: "<<vec1.GetNrows()<<std::endl;
        std::cout<<"Matrix A Col: "<<vec1.GetNcols()<<std::endl;
        std::cout<<"Martix B Row: "<<v_tmp.GetNrows()<<std::endl;
        std::cout<<"Matrix B Col: "<<v_tmp.GetNcols()<<std::endl;
        std::cout<<"Mark B"<<std::endl;
*/
        TMatrixD v_result = vec1*v_tmp;

/*
        std::cout<<"Martix Row: "<<v_result.GetNrows()<<std::endl;
        std::cout<<"Matrix Col: "<<v_result.GetNcols()<<std::endl;
*/
        return 0.5*(v_result[0][0]);
}
/*
double NTupleGetTrackProb(Recon){
        if()
}
*/
//
void
NTupleHelper::fillExtras( vector<FSParticle*> PFOs, string cat, const int type){

	const double eps =1e-6; //Why this value
	sort(PFOs.begin(), PFOs.end(), Sort_PFOs_E);
	vector<TLorentzVector> p4list;
	vector<double>         charge, vecVD, vecVZ, vecVDSIG, vecVZSIG, vecNTRK, vecChi2, vecNDOF, vecProb;	
	//
	for (unsigned int j = 0; j < PFOs.size(); j++){
		FSParticle* fsp = PFOs[j];
		//
		double d0=0; double z0=0;
		double nsigd    = -99999;
		double nsigz    = -99999;
                double chi2 = 0; double ndof =0;
                double prob = 0; double ntrk = 0;  
		if(fabs(fsp->charge())>0.1){
                        nsigd =0 ; nsigz =0; chi2 =0; prob =0; ndof =0;
			TrackVec   tckvec = (fsp->pfo())->getTracks(); ntrk = double(tckvec.size());
			if ( tckvec.size()>0){
                            for(unsigned int k=0; k< tckvec.size();k++){
				d0       += tckvec[k]->getD0();
				z0       += tckvec[k]->getZ0();
				double deltad0  = TMath::Sqrt(tckvec[k]->getCovMatrix()[0]);
				double deltaz0  = TMath::Sqrt(tckvec[k]->getCovMatrix()[9]);
/*
				nsigd    += d0/(deltad0+eps);
				nsigz    += z0/(deltaz0+eps);
*/
                                nsigd    = d0/(deltad0);
                                nsigz    = z0/(deltaz0);
                                ndof     += tckvec[k]->getNdf();
                                chi2     += tckvec[k]->getChi2();
                                prob     += GetTrackProb(tckvec[k]);
                            }
			}
		
                } 
                //
                p4list.push_back(fsp->rawFourMomentum());
                charge.push_back(fsp->charge());
                vecVD.push_back(d0);
                vecVZ.push_back(z0);
                vecVDSIG.push_back(nsigd);
                vecVZSIG.push_back(nsigz);
                vecNTRK.push_back(ntrk);
                vecChi2.push_back(chi2);
                vecNDOF.push_back(ndof);
                vecProb.push_back(prob);  
	}
	//
	string index = "elec_idx";
	if      ( type == 11 ) index = "elec_idx";
	else if ( type == 13 ) index = "muon_idx";
	else if ( type == 22 ) index ="gamma_idx";
	else {
		cout<<"only for photon, muon or electron, but it is  "<< type<<endl;
		exit(1);
	}
	//
	fill4Momentum( index, cat , p4list,   PFOs.size());
	//
	if( type != 22 ){
                fillArray  ( concatName(cat, "charge"),  index, charge, PFOs.size());
		fillArray  ( concatName(cat, "Ntrk"),    index,  vecNTRK, PFOs.size());
                fillArray  ( concatName(cat,  "d0"),  index,  vecVD, PFOs.size());
                fillArray  ( concatName(cat,  "z0"),  index,  vecVZ, PFOs.size());
		fillArray  ( concatName(cat,  "d0sig"),  index,  vecVDSIG, PFOs.size());
		fillArray  ( concatName(cat,  "z0sig"),  index,  vecVZSIG, PFOs.size());
                fillArray  ( concatName(cat,  "NDOF"),  index,  vecNDOF, PFOs.size());
                fillArray  ( concatName(cat,  "Chi2"),  index,  vecChi2, PFOs.size());
                fillArray  ( concatName(cat,  "NDOF"),  index,  vecNDOF, PFOs.size());
                fillArray  ( concatName(cat,  "Prob"),  index,  vecProb, PFOs.size());
	}
	//
	FreeAll(p4list);
	FreeAll(charge);
	FreeAll( vecVD);
	FreeAll( vecVZ);
        FreeAll( vecVDSIG);
        FreeAll( vecVZSIG);
        FreeAll( vecNDOF);
        FreeAll( vecChi2);
        FreeAll( vecNTRK);
        FreeAll( vecProb);
}
//
double NTupleHelper::JetCharge(ReconstructedParticle* jet, const double kappa){
		ReconstructedParticleVec vPart = jet->getParticles();
		int nPart = vPart.size(), id = -1 ; 
		TVector3 MOMENTUM  = TVector3(jet->getMomentum());
		double P           = 1; 
		if( id == 0 ) P    = jet->getEnergy();
		if( id == 1 ) P    = MOMENTUM.Pt();
		if( id == 2 ) P    = MOMENTUM.Mag();
		if(nPart>0){
			double charge = 0 , wate = 0 ;
			for (int k=0 ; k < nPart ; k++ ) {
				TVector3 momentum   = TVector3(vPart[k]->getMomentum());
				double c            = vPart[k]->getCharge();
				//cout<<c<<endl;
				if( fabs(c)<0.1 || fabs(c)>1.1)     continue;	
				double   p          = 0.0; 
				if( id <  0 ) p     = TMath::Cos(momentum.Angle(MOMENTUM));
				if( id == 0 ) p     = vPart[k]->getEnergy();
				if( id == 1 ) p     = momentum.Pt()/P;
				if( id == 2 ) p     = momentum.Mag();
				//if( fabs(p)<1.0 ) continue;	
				//
				double w  = pow( fabs(p), kappa );
				charge   += ( c*w );
				wate     += (  w  );
			}

			//cout << "wate/kappa = " << wate << "/" << kappa << endl;
			if( id <  0 ) charge = charge;//wate; 
			if( id == 0 ) charge = charge;//wate; 
			if( id == 1 ) charge = charge;//wate; 
			if( id == 2 ) charge = charge;//wate; 
			return charge;
		}
		return 9999.0;
}
//
void 
NTupleHelper::fillJet(FSParticle * fsp, int index, string tag , int m_full, double kappa, LCRelationNavigator *navMCTL_jet){
	ReconstructedParticle * recPart = fsp->pfo();
	ReconstructedParticleVec vPart  = recPart->getParticles();
	//
	int ntrk = 0;
	int nclu = 0;
	int nPFO = vPart.size();
	double d0=0., z0=0., deltad0=0., deltaz0=0., nsigd0=0., nsigz0=0., prob=0, ntrktrk=0, chi2=0., ndof =0;
        vector<double> pfo_energy;
        vector<double> pfo_px;
        vector<double> pfo_py;
        vector<double> pfo_pz;
        vector<double> pfo_charge;
        vector<double> pfo_d0;
        vector<double> pfo_z0;
        vector<double> pfo_sigmad0;
        vector<double> pfo_sigmaz0;
        vector<double> pfo_prob;
        vector<double> pfo_ntrk;
        vector<double> pfo_chi2;
        vector<double> pfo_ndof;
        vector<double> pfo_pdg;
	if( nPFO>0 ){
		for (int k=0 ; k < nPFO ; k++ ) {
			TrackVec                 tckvec = vPart[k]->getTracks();
			ClusterVec               cluvec = vPart[k]->getClusters();
			ntrk   += tckvec.size();
			nclu   += cluvec.size();
                        pfo_energy.push_back(vPart[k]->getEnergy());   
                        pfo_px.push_back(vPart[k]->getMomentum()[0]);
                        pfo_py.push_back(vPart[k]->getMomentum()[1]);
                        pfo_pz.push_back(vPart[k]->getMomentum()[2]);
                        pfo_charge.push_back(vPart[k]->getCharge());
                        ntrktrk = 0;
			if( m_full>0 ){
				if (tckvec.size() > 0) {
                                        d0=0.; z0=0.; deltad0=0.; deltaz0=0.; nsigd0=0.; nsigz0=0; prob=0; ntrktrk=double(tckvec.size()); chi2=0.; ndof =0;                   
					for (unsigned int j=0 ; j < tckvec.size() ; j++ ) {
/*This block seems very strange*/

						d0      += tckvec[j]->getD0();
						z0      += tckvec[j]->getZ0();
						deltad0  = TMath::Sqrt(tckvec[j]->getCovMatrix()[0]);
						deltaz0  = TMath::Sqrt(tckvec[j]->getCovMatrix()[9]);
						nsigd0  += tckvec[j]->getD0()/deltad0;
						nsigz0  += tckvec[j]->getZ0()/deltaz0;
                                                prob += (GetTrackProb(tckvec[j]));
                                                ndof += double(tckvec[j]->getNdf());
                                                chi2 += tckvec[j]->getChi2();                                                    
                                                 
					}
				}
			}
                        
                        if(tckvec.size() > 0){
                                pfo_sigmad0.push_back(nsigd0);
                                pfo_sigmaz0.push_back(nsigz0);
                                pfo_d0.push_back(d0);     
                                pfo_z0.push_back(z0);
                                pfo_prob.push_back(prob);
                                pfo_chi2.push_back(chi2);
                                pfo_ndof.push_back(ndof);
                        }else{
                                pfo_sigmad0.push_back(-1000.0);
                                pfo_sigmaz0.push_back(-1000.0);
                                pfo_d0.push_back(-1000.0);
                                pfo_z0.push_back(-1000.0); 
                                pfo_prob.push_back(-1000);
                                pfo_chi2.push_back(-1000);
                                pfo_ndof.push_back(-1000);
                        }

                        pfo_ntrk.push_back(ntrktrk);

                        int pfo_pdgid = 0;
                        if(navMCTL_jet){
                              LCObjectVec vecMCTL            = navMCTL_jet->getRelatedToObjects(vPart[k]);
                              if (vecMCTL.size() > 0) {
                                      for(unsigned int kp=0; kp< vecMCTL.size(); kp++){
                                              EVENT::MCParticle* mcp = dynamic_cast<EVENT::MCParticle *>(vecMCTL[kp]);
                                              pfo_pdgid=abs(mcp->getPDG());
                                      }
                              }

                        }

                        if(pfo_pdgid == 22||fabs(pfo_pdgid)==11||fabs(pfo_pdgid)==13||fabs(pfo_pdgid)==211||fabs(pfo_pdgid)==321||fabs(pfo_pdgid)==2212)pfo_pdg.push_back((double)pfo_pdgid);
                        else pfo_pdg.push_back(0);
		}
		d0      /= ntrk;
		z0      /= ntrk;
		nsigd0  /= ntrk;
		nsigz0  /= ntrk;
	}
	//
	if(0) printf("Ntrk = %3d, Nclu = %3d, Npar = %3d\n", ntrk, nclu, nPFO);
	//
	double    energy     = recPart->getEnergy();
	//double  charge     = recPart->getCharge();
	double    charge     = JetCharge(recPart, kappa);
	double   mass       = recPart->getMass();
	TVector3 momentum   = TVector3(recPart->getMomentum());
	HepLorentzVector p4mom =HepLorentzVector (momentum.X(), momentum.Y(), momentum.Z(),energy);
	double   momentumM  = momentum.Mag();
	double   rapidity   = 0.5*TMath::Log((energy+momentum.Pz())/(energy-momentum.Pz()));
	double   cosTheta   = momentum.CosTheta();
/*
        int pfo_pdgid = 0;
        if(navMCTL_jet){
              LCObjectVec vecMCTL            = navMCTL_jet->getRelatedToObjects(recPart);
              if (vecMCTL.size() > 0) {
                      for(unsigned int k=0; k< vecMCTL.size(); k++){
                              EVENT::MCParticle* mcp = dynamic_cast<EVENT::MCParticle *>(vecMCTL[k]);
                              pfo_pdgid=abs(mcp->getPDG());
                      }
              }
        }
        double pfo_pdg = 0;
        if(pfo_pdgid == 22||fabs(pfo_pdgid)==11||fabs(pfo_pdgid)==13||fabs(pfo_pdgid)==211||fabs(pfo_pdgid)==321||fabs(pfo_pdgid)==2212)pfo_pdg = (double)pfo_pdgid;
*/        

	//double   sphericity = fsp->sphericity(); 
	//
        fillArray(concatName(tag,"PFOEn",index),"idx_jetpfo",pfo_energy,nPFO);
        fillArray(concatName(tag,"PFOPx",index),"idx_jetpfo",pfo_px,nPFO); 
        fillArray(concatName(tag,"PFOPy",index),"idx_jetpfo",pfo_py,nPFO);
        fillArray(concatName(tag,"PFOPz",index),"idx_jetpfo",pfo_pz,nPFO);
        fillArray(concatName(tag,"PFOCharge",index),"idx_jetpfo",pfo_charge,nPFO);
        fillArray(concatName(tag,"PFOD0",index),"idx_jetpfo",pfo_d0,nPFO);
        fillArray(concatName(tag,"PFOZ0",index),"idx_jetpfo",pfo_z0,nPFO);
        fillArray(concatName(tag,"PFOSIGMAD0",index),"idx_jetpfo",pfo_sigmad0,nPFO);
        fillArray(concatName(tag,"PFOSIGMAZ0",index),"idx_jetpfo",pfo_sigmaz0,nPFO);
        fillArray(concatName(tag,"PFOProb",index),"idx_jetpfo",pfo_prob, nPFO);
        fillArray(concatName(tag,"PFOChi2",index),"idx_jetpfo",pfo_chi2, nPFO);
        fillArray(concatName(tag,"PFONtrk",index),"idx_jetpfo",pfo_ntrk, nPFO);
        fillArray(concatName(tag,"PFONDOF",index),"idx_jetpfo",pfo_ndof, nPFO);
        fillArray(concatName(tag,"PFOPDGID",index),"idx_jetpfo",pfo_pdg,nPFO);

	if( m_full > 0 ){
		fillDouble(concatName(tag,"VtxR"     ,  index), d0              );
		fillDouble(concatName(tag,"VtxZ"     ,  index), z0              );
		fillDouble(concatName(tag,"VtxSigR"  ,  index), nsigd0          );
		fillDouble(concatName(tag,"VtxSigZ"  ,  index), nsigz0          );
		fillDouble(concatName(tag,"Btag"     ,  index), fsp->btag()     );
		fillDouble(concatName(tag,"Ctag"     ,  index), fsp->ctag()     );
		fillDouble(concatName(tag,"BCtag"    ,  index), fsp->bctag()    );
		fillDouble(concatName(tag,"Cat"      ,  index), fsp->cat()      );
	}
	fillDouble(concatName(tag,"ntrk"      ,  index), ntrk            );
	fillDouble(concatName(tag,"nclu"      ,  index), nclu            );
	fillDouble(concatName(tag,"charge"    ,  index), charge          );
	fillDouble(concatName(tag,"nPFO"      ,  index), nPFO            );
	fillDouble(concatName(tag,"mass"      ,  index), mass            );
	fillDouble(concatName(tag,"En"        ,  index), energy          );
	fillDouble(concatName(tag,"Px"        ,  index), momentum.X()    );
	fillDouble(concatName(tag,"Py"        ,  index), momentum.Y()    );
	fillDouble(concatName(tag,"Pz"        ,  index), momentum.Z()    );
	fillDouble(concatName(tag,"Pt"        ,  index), momentum.Pt()   );
	fillDouble(concatName(tag,"Ptot"      ,  index), momentumM       );
	fillDouble(concatName(tag,"Rapidity"  ,  index), rapidity        );
	fillDouble(concatName(tag,"cosTheta"  ,  index), cosTheta        );
	//fillDouble(concatName(tag,"Sphericity",  index), sphericity      );
	double pdgid=0;
	HepLorentzVector p4(0,0,0,0);
	MCParticle * mcp= fsp->mcp();
	if( mcp ) {
		pdgid= mcp->getPDG();

		p4=HepLorentzVector (mcp->getMomentum()[0], mcp->getMomentum()[1], mcp->getMomentum()[2], mcp->getEnergy() );

	} 
	fillDouble(concatName(tag,"PDGID"      ,  index),   pdgid        );
	fillDouble(concatName(tag,"McPx"       ,  index),   p4.px()      );
	fillDouble(concatName(tag,"McPy"       ,  index),   p4.py()      );
	fillDouble(concatName(tag,"McPz"       ,  index),   p4.pz()      );
	fillDouble(concatName(tag,"McEn"       ,  index),   p4.e ()      );
	fillDouble(concatName(tag,"AngleRecMc" ,  index),   p4.angle(p4mom.v()));
}
void 
NTupleHelper::fillTracks( vector<Track*> tckvec ){

	vector<double> VecD0, VecZ0, VecSIGD0, VecSIGZ0, VecProb, VecChi2, VecNDOF;//,VecCos,VecP;
	double  d0=9990.,z0=9990.,deltad0=0.,deltaz0=0.,nsigd0=9990.,nsigz0=9990.;//,cos=999,pp=-999;
	for (unsigned int j=0 ; j < tckvec.size() ; j++ ) {
		d0       = tckvec[j]->getD0();
		z0       = tckvec[j]->getZ0();
		deltad0  = TMath::Sqrt(tckvec[j]->getCovMatrix()[0]);
		deltaz0  = TMath::Sqrt(tckvec[j]->getCovMatrix()[9]);
		nsigd0   = tckvec[j]->getD0()/deltad0;
		nsigz0   = tckvec[j]->getZ0()/deltaz0;
		//cos      = tckvec[j]->getTanLambda();
	     	//cos      = 1/pow(1+pow(cos,2),0.5)*(cos/fabs(cos));
	      	
		VecD0 .push_back(d0);  VecSIGD0.push_back(deltad0);
		VecZ0 .push_back(z0);  VecSIGZ0.push_back(deltaz0);
                VecProb.push_back(GetTrackProb(tckvec[j]));
                VecChi2.push_back(tckvec[j]->getChi2());
                VecNDOF.push_back(double(tckvec[j]->getNdf()));

		//VecCos.push_back(cos);
		//VecP  .push_back(pp);
	}
	
	fillArray ("trkD0"    , "idx_trk", VecD0    , tckvec.size());
	fillArray ("trkZ0"    , "idx_trk", VecZ0    , tckvec.size());
        fillArray ("trkSIGD0",  "idx_trk", VecSIGD0 , tckvec.size());
        fillArray ("trkSIGZ0",  "idx_trk", VecSIGZ0 , tckvec.size());
        fillArray ("trkProb"  , "idx_trk", VecProb  , tckvec.size());
        fillArray ("trkChi2"  , "idx_trk", VecChi2  , tckvec.size());
        fillArray ("trkNDOF"  , "idx_trk", VecNDOF  , tckvec.size());

	//fillArray ("trkCos"   , "idx_trk", VecCos   , tckvec.size());
}

void 
NTupleHelper::fillPFO(ReconstructedParticle * recPart, FSParticle * fsp, int index, string tag, int m_full ){
	TrackVec   tckvec = recPart->getTracks();
	ClusterVec cluvec = recPart->getClusters();
	int ntrk = tckvec.size();
	int nclu = cluvec.size();
	double  d0=9990.,z0=9990.,deltad0=0.,deltaz0=0.,nsigd0=9990.,nsigz0=9990.;
	if (m_full > 0 && ntrk > 0) {
		d0=0., z0=0., nsigd0=0., nsigz0=0.;
		for (unsigned int j=0 ; j < tckvec.size() ; j++ ) {
/* This block seems very strange*/

			d0      += tckvec[j]->getD0();
			z0      += tckvec[j]->getZ0();
			deltad0  = TMath::Sqrt(tckvec[j]->getCovMatrix()[0]);
			deltaz0  = TMath::Sqrt(tckvec[j]->getCovMatrix()[9]);
			nsigd0  += tckvec[j]->getD0()/deltad0;
			nsigz0  += tckvec[j]->getZ0()/deltaz0;
                      
		}
		d0      /= ntrk;
		z0      /= ntrk;
		nsigd0  /= ntrk;
		nsigz0  /= ntrk;
	}
	//
	double   leptonType = fsp->leptonType(); 
	double   energy     = recPart->getEnergy();
	double   charge     = recPart->getCharge();
	double   mass       = recPart->getMass();
	TVector3 momentum   = TVector3(recPart->getMomentum());
	double   momentumM  = momentum.Mag();
	double   rapidity   = 0.5*TMath::Log((energy+momentum.Pz())/(energy-momentum.Pz()));
	double   cosTheta   = momentum.CosTheta();
	double   ecalEnergy = 0;
	double   hcalEnergy = 0;
	double   yokeEnergy = 0;
	double   totalCalEnergy = 0;
        double   prob = 0;
        double   chi2 = 0;
        double   ndof = 0;
/*
        double   d0 = 0; 
        double   z0 = 0;
        double   nsigd0=0;
        double   nsigz0=0;
*/
	int nHits = 0;
	if ( m_full>0  && energy > 0) {
		std::vector<lcio::Cluster*> clusters = recPart->getClusters();
		for (std::vector<lcio::Cluster*>::const_iterator iCluster=clusters.begin();iCluster!=clusters.end();++iCluster) {
			ecalEnergy += (*iCluster)->getSubdetectorEnergies()[0];
			hcalEnergy += (*iCluster)->getSubdetectorEnergies()[1];
			yokeEnergy += (*iCluster)->getSubdetectorEnergies()[2];
			ecalEnergy += (*iCluster)->getSubdetectorEnergies()[3];
			hcalEnergy += (*iCluster)->getSubdetectorEnergies()[4];
			CalorimeterHitVec calHits = (*iCluster)->getCalorimeterHits();
			nHits += calHits.size();
		}
		totalCalEnergy = ecalEnergy + hcalEnergy;
	}
	//
	if ( m_full>0 ) {
		fillDouble(concatName(tag,"ntrk"     ,  index), ntrk);
		fillDouble(concatName(tag,"nclu"     ,  index), nclu);
		fillDouble(concatName(tag,"VtxR"     ,  index), d0);
		fillDouble(concatName(tag,"VtxZ"     ,  index), z0);
		fillDouble(concatName(tag,"ecal"     ,  index), ecalEnergy  );
		fillDouble(concatName(tag,"hcal"     ,  index), hcalEnergy  );
		fillDouble(concatName(tag,"nHits"    ,  index), nHits);
		fillDouble(concatName(tag,"totCalEn" ,  index), totalCalEnergy);
                if(charge != 0){
                       if (tckvec.size() > 0) {
                             prob=0; chi2=0.; ndof =0;
                             for (unsigned int j=0 ; j < tckvec.size() ; j++ ) {
/*
                                     d0      += tckvec[j]->getD0();
                                     z0      += tckvec[j]->getZ0();
                                     deltad0  = TMath::Sqrt(tckvec[j]->getCovMatrix()[0]);
                                     deltaz0  = TMath::Sqrt(tckvec[j]->getCovMatrix()[9]);
                                     nsigd0  += tckvec[j]->getD0()/deltad0;
                                     nsigz0  += tckvec[j]->getZ0()/deltaz0;
*/
                                     prob += (GetTrackProb(tckvec[j]));
                                     ndof += double(tckvec[j]->getNdf());
                                     chi2 += tckvec[j]->getChi2();
                             }
                      } 
                }
	}
	fillDouble(concatName(tag,"charge"   ,  index), charge);
	fillDouble(concatName(tag,"mass"     ,  index), mass  );
	fillDouble(concatName(tag,"En"       ,  index), energy);
	fillDouble(concatName(tag,"Px"       ,  index), momentum.X());
	fillDouble(concatName(tag,"Py"       ,  index), momentum.Y());
	fillDouble(concatName(tag,"Pz"       ,  index), momentum.Z());
	fillDouble(concatName(tag,"Pt"       ,  index), momentum.Pt());
	fillDouble(concatName(tag,"Ptot"     ,  index), momentumM   );
	fillDouble(concatName(tag,"LepType"  ,  index), leptonType);
	fillDouble(concatName(tag,"Rapidity" ,  index), rapidity    );
	fillDouble(concatName(tag,"cosTheta" ,  index), cosTheta    );
        fillDouble(concatName(tag,"D0" ,  index), d0);
        fillDouble(concatName(tag,"Z0" ,  index), z0);
        fillDouble(concatName(tag,"SIGMAD0" ,  index), nsigd0);
        fillDouble(concatName(tag,"SIGMAZ0" ,  index), nsigz0);
        fillDouble(concatName(tag,"Prob" ,  index), prob);
        fillDouble(concatName(tag,"Chi2" ,  index), chi2);
        fillDouble(concatName(tag,"NDOF" ,  index), ndof);
	double pdgid=0, mcten=0, mctpx=0, mctpy=0, mctpz=0;
	MCParticle * mcp= fsp->mcp();
	if( mcp ) {
		mcten  = mcp->getEnergy();
		TVector3  p3  = TVector3(mcp->getMomentum());
		mcten  = mcp->getEnergy();
		mctpx  = p3.X(); 
		mctpy  = p3.Y(); 
		mctpz  = p3.Z(); 
		pdgid= mcp->getPDG();
	}	
	fillDouble(concatName(tag,"PDGID"    ,  index),   pdgid  );
	fillDouble(concatName(tag,"MCTEN"    ,  index),   mcten  );
	fillDouble(concatName(tag,"MCTPX"    ,  index),   mctpx  );
	fillDouble(concatName(tag,"MCTPY"    ,  index),   mctpy  );
	fillDouble(concatName(tag,"MCTPZ"    ,  index),   mctpz  );
}
void 
NTupleHelper::fillPFOs(vector<ReconstructedParticle*> pfos){

	vector<double> VecEn, VecPp, VecPx, VecPy, VecPz, VecCs, VecCh, VecProb, VecNDOF, VecChi2;
	for (unsigned int j = 0; j < pfos.size(); j++){
		ReconstructedParticle * pfo= pfos[j];
		double   energy     = pfo->getEnergy();
		double   charge     = pfo->getCharge();
		TVector3 momentum   = TVector3(pfo->getMomentum());
		double   momentumM  = momentum.Mag();
		double   cosTheta   = momentum.CosTheta();
		//
		VecEn.push_back(energy);
		VecPp.push_back(momentumM);
		VecPx.push_back(momentum.X());
		VecPy.push_back(momentum.Y());
		VecPz.push_back(momentum.Z());
		VecCs.push_back(cosTheta);
		VecCh.push_back(charge);
                if(charge ==0){VecProb.push_back(0); VecChi2.push_back(0); VecNDOF.push_back(0);}
                else {
                     
                }
	}
	//
	fillArray ("Pfo_En"    , "idx_pfo", VecEn    , pfos.size());
	fillArray ("Pfo_Pp"    , "idx_pfo", VecPp    , pfos.size());
	fillArray ("Pfo_Px"    , "idx_pfo", VecPx    , pfos.size());
	fillArray ("Pfo_Py"    , "idx_pfo", VecPy    , pfos.size());
	fillArray ("Pfo_Pz"    , "idx_pfo", VecPz    , pfos.size());
	fillArray ("Pfo_Cos"   , "idx_pfo", VecCs    , pfos.size());
	fillArray ("Pfo_Charge", "idx_pfo", VecCh    , pfos.size());

}
void 
NTupleHelper::fillPFO(FSParticle * fsPart, int index, string tag){
	//
	double   energy     = fsPart->energy();
	double   charge     = fsPart->charge();
	TVector3 momentum   = TVector3((fsPart->rawFourMomentum()).Vect());
	double   momentumM  = momentum.Mag();
	double   rapidity   = 0.5*TMath::Log((energy+momentum.Pz())/(energy-momentum.Pz()));
	double   cosTheta   = momentum.CosTheta();
	//
	fillDouble(concatName(tag,"charge"   ,  index), charge);
	fillDouble(concatName(tag,"En"       ,  index), energy);
	fillDouble(concatName(tag,"Px"       ,  index), momentum.X());
	fillDouble(concatName(tag,"Py"       ,  index), momentum.Y());
	fillDouble(concatName(tag,"Pz"       ,  index), momentum.Z());
	fillDouble(concatName(tag,"Pt"       ,  index), momentum.Pt());
	fillDouble(concatName(tag,"Ptot"     ,  index), momentumM   );
	fillDouble(concatName(tag,"Rapidity" ,  index), rapidity    );
	fillDouble(concatName(tag,"cosTheta" ,  index), cosTheta    );
}

//
void
NTupleHelper::fill4Momentum(int index, string tag, const TLorentzVector& p){
	fillDouble(concatName(tag,"Px",index), p.Px());
	fillDouble(concatName(tag,"Py",index), p.Py());
	fillDouble(concatName(tag,"Pz",index), p.Pz());
	fillDouble(concatName(tag,"En",index), p.E() );
}
//
void
NTupleHelper::fill4Momentum(string tag, const TLorentzVector& p){
	fillDouble(concatName(tag,"Px",0), p.Px());
	fillDouble(concatName(tag,"Py",0), p.Py());
	fillDouble(concatName(tag,"Pz",0), p.Pz());
	fillDouble(concatName(tag,"En",0), p.E() );
}
//
void
NTupleHelper::fill4Momentum(int index, string tag){
	fillDouble(concatName(tag,"Px",index), 0.0);
	fillDouble(concatName(tag,"Py",index), 0.0);
	fillDouble(concatName(tag,"Pz",index), 0.0);
	fillDouble(concatName(tag,"En",index), 0.0);
}
//
// ********************************************************************
//    FOUR MOMENTA
// ********************************************************************
void
NTupleHelper::fill4Momentum(string index_name, string tag, const vector<TLorentzVector>& p, const int size){

	int size_loc=size;
	if(size > maxsize){
		//printf("too many entries %3d (maximum is 100), please check\n", size);
		//exit(1);
		size_loc=maxsize;
	}	
	if (m_bookingStage && !containsEntry(index_name)){
		char Name[20], Description[20];
		sprintf(Name,"%s",index_name.c_str());
		sprintf(Description,"%s/I",index_name.c_str());
		m_Tree->Branch(Name , &m_ntupleIntMap[index_name], Description);	
	}
	if (!m_bookingStage && !containsEntry(index_name)){
		cout << "NTUPLEHELPER:  Variable " << index_name << " has not been booked." << endl;
		exit(0);
	}
	m_IntMap   [index_name] = size_loc;
	//
	//
	string combname[4]={"Px","Py","Pz","En"}, name;
	for(int i=0; i<4; i++){
		name=concatName(tag,combname[i]);
		if (m_bookingStage && !containsEntry(name)){
			char Name[20], Description[20];
			sprintf(Name       ,"%s[%d]"  , name.c_str(), maxsize);
			sprintf(Description,"%s[%d]/D", name.c_str(), maxsize);
			m_ntupleArrayMap[name]= new double[maxsize];
			m_Tree->Branch(Name, m_ntupleArrayMap[name], Description);	
		}
		if (!m_bookingStage && !containsEntry(name)){
			cout << "NTUPLEHELPER:  Variable " << name << " has not been booked." << endl;
			exit(0);
		}
		m_arrayMap [name] = new double[maxsize];
		memset(m_arrayMap[name], 0, sizeof(double)*maxsize);
		m_arraySize[name] = size_loc;
		for(int j=0; j<size_loc; j++){
				if(i==0) m_arrayMap[name][j] = p[j].Px();
				if(i==1) m_arrayMap[name][j] = p[j].Py();
				if(i==2) m_arrayMap[name][j] = p[j].Pz();
				if(i==3) m_arrayMap[name][j] = p[j].E ();
		}
	}
}

// ********************************************************************
//    HELPER FUNCTIONS
// ********************************************************************
NTupleHelper::NTupleHelper(TTree* Tree, TLorentzVector p4){
	m_Tree         = Tree;
	p4psi          =   p4;
	m_bookingStage = true;
	FreeAll(m_doubleMap);
	FreeAll(m_IntMap   );
	FreeAll(m_arrayMap );
	FreeAll(m_arraySize);
	if( m_Tree == NULL )
		cout    << "ERROR:  null tree pointer -- "
			<< "check for duplicate final states in configuration"
			<< endl; assert( m_Tree != NULL );
}

NTupleHelper::~NTupleHelper(){
	m_Tree->Write();
	for (map<string,double*>::iterator mapItr = m_ntupleArrayMap.begin();
			mapItr != m_ntupleArrayMap.end(); mapItr++){
		delete []mapItr->second;
	}
	delete m_Tree;
}

void
NTupleHelper::write(){
	
	for (map<string,double>::iterator mapItr = m_doubleMap.begin();
			mapItr != m_doubleMap.end(); mapItr++){
		m_ntupleDoubleMap[mapItr->first] = mapItr->second;
	}
	
	for (map<string,int>::iterator mapItr = m_IntMap.begin();
			mapItr != m_IntMap.end(); mapItr++){
		m_ntupleIntMap[mapItr->first] = mapItr->second;
	}
	
	for (map<string,double*>::iterator mapItr = m_arrayMap.begin();
			mapItr != m_arrayMap.end(); mapItr++){
		for( int i=0; i< maxsize; i++)
		{
			m_ntupleArrayMap[mapItr->first][i]=mapItr->second[i];
		}
	}

	m_Tree->Fill();

	for (map<string,double*>::iterator mapItr = m_arrayMap.begin();
			mapItr != m_arrayMap.end(); mapItr++){
		delete[] mapItr->second;
	}

	m_bookingStage = false;
	
}


void NTupleHelper::fillDouble(string name, double value){
	if (m_bookingStage && !containsEntry(name)){
		char Name[20], Description[20];
		sprintf(Name,"%s",name.c_str());
		sprintf(Description,"%s/D",name.c_str());
		m_Tree->Branch(Name , &m_ntupleDoubleMap[name], Description);	
	}
	if (!m_bookingStage && !containsEntry(name)){
		cout << "NTUPLEHELPER:  Variable " << name << " has not been booked." << endl;
		exit(0);
	}
	m_doubleMap[name] = value;
}

void NTupleHelper::fillLong(string name, int value){
	if (m_bookingStage && !containsEntry(name)){
		char Name[20], Description[20];
		sprintf(Name,"%s",name.c_str());
		sprintf(Description,"%s/I",name.c_str());
		m_Tree->Branch(Name   , &m_ntupleIntMap[name], Description);	
	}
	if (!m_bookingStage && !containsEntry(name)){
		cout << "NTUPLEHELPER:  Variable " << name << " has not been booked." << endl;
		exit(0);
	}
	m_IntMap[name] = value;
}


void 
NTupleHelper::fillArray(string name, string index_name, double* value, int size){
	
	int size_loc=size;
	if(size >maxsize){
		//printf("too many entries %3d (maximum is 30), please check\n", size);
		//exit(1);
		size_loc=maxsize;
	}	
	if (m_bookingStage && !containsEntry(index_name)){
		char Name[20], Description[20];
		sprintf(Name,"%s",index_name.c_str());
		sprintf(Description,"%s/I",index_name.c_str());
		m_Tree->Branch(Name, &m_ntupleIntMap[index_name], Description);
	}
	
	m_IntMap[index_name]=size_loc;
	m_arraySize[name   ]=size_loc;

	if (m_bookingStage && !containsEntry(name)){
		char Name[20], Description[20];
		sprintf(Name       ,"%s[%d]"  , name.c_str(), maxsize);
		sprintf(Description,"%s[%d]/D", name.c_str(), maxsize);
		m_ntupleArrayMap[name]= new double[maxsize];
		m_Tree->Branch(Name, m_ntupleArrayMap[name], Description);	
	}
	
	if (!m_bookingStage && !containsEntry(name)){
		cout << "NTUPLEHELPER:  Variable " << name << " has not been booked." << endl;
		exit(0);
	}
	
	if (!m_bookingStage && !containsEntry(index_name)){
		cout << "NTUPLEHELPER:  Variable " << index_name << " has not been booked." << endl;
		exit(0);
	}
        
	m_arrayMap[name]= new double[maxsize];
	memset(m_arrayMap[name], 0, sizeof(double)*maxsize);
	for(int i=0; i<size_loc; i++){
		m_arrayMap[name][i] = *(value+i);
	}
}

void 
NTupleHelper::fillArray(string name, string index_name, vector<double> value, int size){

	int size_loc=size;
	if(size >maxsize){
		//printf("too many entries %3d (maximum is 30), please check\n", size);
		//exit(1);
		size_loc=maxsize;
	}	
	if (m_bookingStage && !containsEntry(index_name)){
		char Name[20], Description[20];
		sprintf(Name,"%s",index_name.c_str());
		sprintf(Description,"%s/I",index_name.c_str());
		m_Tree->Branch(Name, &m_ntupleIntMap[index_name], Description);	
	}
	
	m_IntMap[index_name]=size_loc;
	m_arraySize[name   ]=size_loc;

	if (m_bookingStage && !containsEntry(name)){
		char Name[20], Description[20];
		sprintf(Name       ,"%s[%d]"  , name.c_str(), maxsize);
		sprintf(Description,"%s[%d]/D", name.c_str(), maxsize);
		m_ntupleArrayMap[name]= new double[maxsize];
		m_Tree->Branch(Name, m_ntupleArrayMap[name], Description);	
	}
	
	if (!m_bookingStage && !containsEntry(name)){
		cout << "NTUPLEHELPER:  Variable " << name << " has not been booked." << endl;
		exit(0);
	}
	
	if (!m_bookingStage && !containsEntry(index_name)){
		cout << "NTUPLEHELPER:  Variable " << index_name << " has not been booked." << endl;
		exit(0);
	}

	m_arrayMap[name]= new double[maxsize];
	memset(m_arrayMap[name], 0, sizeof(double)*maxsize);
	for(int i=0; i<size_loc; i++){
		m_arrayMap[name][i] = value[i];
	}
}

bool
NTupleHelper::containsEntry(string name){
	map<string,double>::iterator mapItr1 = m_doubleMap.find(name);
	if (mapItr1 != m_doubleMap.end()) return true;
	
	map<string,int>::iterator mapItr2 = m_IntMap.find(name);
	if (mapItr2 != m_IntMap.end()   ) return true;
	
	map<string,double*>::iterator mapItr3 = m_arrayMap.find(name);
	if (mapItr3 != m_arrayMap.end() ) return true;
	
	return false;
}

string 
NTupleHelper::concatName(string tag, string base, int index){
	stringstream name;
	name << tag;
	name << base;
	if( index>0){
		name << "P";
		name << index;
	}
	return name.str();
}

string 
NTupleHelper::concatName(string tag, string base){
	stringstream name;
	name << tag;
	name << base;
	return name.str();
}
/*
void
NTupleHelper::fillMCZZ(MCTruthHelper* m_mcTruthHelper, LCCollection    * mcParticleCol){
     double ZDecay[2]; 
     double ZMass[2] ;
     m_mcTruthHelper->GetZDecayInfo(mcParticleCol,ZDecay,ZMass);
     fillArray("ZDecay","indexZ",ZDecay,2);
     fillArray("ZMass","indexZ",ZMass,2) ;
}
*/
void
NTupleHelper::fillMCTruth(MCTruthHelper* mc, vector<int> pdgid_list){
                const int limit = 299;
                vector<MCParticleImpl*> mcPList    = mc->AllParticleList();
                int    m_numParticle=0;
                double mc_pdgid[1000], mc_motheridx[1000], mc_trkidx[1000];
                double mc_px[1000], mc_py[1000], mc_pz[1000], mc_pe[1000];
                double mc_nparents[1000];
                double mc_primaryparent[1000];
                for (unsigned int i=0; i < mcPList.size(); i++)
                {
                        bool pdgmatching = false; 
                        if(pdgid_list.size()){
                             for(size_t p=0;p<pdgid_list.size();p++){
                                   if(pdgid_list.at(p) ==  mcPList[i]->getPDG()) {pdgmatching = true;break;}
                             }
                        } 
                        if(pdgid_list.size() && (!pdgmatching)) continue;
                        int pdgid      = mcPList[i]->getPDG();
                        int id         = mcPList[i]->id();

                        if (    mcPList[i]->getGeneratorStatus() == 0      ) continue;                         
                        if (    mcPList[i]->isCreatedInSimulation()        ) continue;
                        if (    mcPList[i]->isBackscatter()                ) continue;
                        if (    mcPList[i]->isOverlay()                    ) continue;
                        if (    mcPList[i]->vertexIsNotEndpointOfParent()  ) continue;

                        int nparents   = mcPList[i]->getParents().size();
                        Int_t mother = 0, mid=0;
                        if (nparents > 0) {
                                MCParticle *Parent = mcPList[i]->getParents()[0];
                                mother             = Parent->getPDG();
                                mid                = Parent->id();
                        }

                         TLorentzVector p4p   = TLorentzVector(mcPList[i]->getMomentum(),mcPList[i]->getEnergy());
                        mc_trkidx   [m_numParticle] = id ;
                        mc_pdgid    [m_numParticle] = pdgid  ;
                        mc_px       [m_numParticle] = p4p.X();
                        mc_py       [m_numParticle] = p4p.Y();
                        mc_pz       [m_numParticle] = p4p.Z();
                        mc_pe       [m_numParticle] = p4p.T();
                        mc_nparents [m_numParticle] = nparents;
                        mc_primaryparent[m_numParticle] = mother;
                        m_numParticle++;
                        if(m_numParticle>limit )
                        {
                                cout<<"m_numParticle >  : "<< limit << " " <<m_numParticle<<endl;
                                break;
                        }
                }
                fillArray ("trkidx"    , "indexmc", (double*)mc_trkidx    , m_numParticle);
                fillArray ("pdgid"     , "indexmc", (double*)mc_pdgid     , m_numParticle);
                fillArray ("mc_Px"     , "indexmc", (double*)mc_px        , m_numParticle);
                fillArray ("mc_Py"     , "indexmc", (double*)mc_py        , m_numParticle);
                fillArray ("mc_Pz"     , "indexmc", (double*)mc_pz        , m_numParticle);
                fillArray ("mc_En"     , "indexmc", (double*)mc_pe        , m_numParticle);
                fillArray ("nparents"  , "indexmc", (double*)mc_nparents  , m_numParticle);
                fillArray ("primaryparent", "indexmc", (double*)mc_primaryparent, m_numParticle);
}

void // this function is not ready due to the complication of whizard MC topology  
NTupleHelper::fillMCTruthDT(MCTruthHelper* mc, FSInfo* fs ){
	//printf("\n\n"); 
	if (fs){
		const int limit = 999;
		vector<MCParticleImpl*> mcPList    = mc->AllParticleList();
		//
		int    m_numParticle=0;
		double mc_pdgid[1000], mc_motheridx[1000], mc_trkidx[1000];
		double mc_px[1000], mc_py[1000], mc_pz[1000], mc_pe[1000];
		for (unsigned int i=0; i < mcPList.size(); i++)
		{
			//int isCreatedInSimulation         = mcPList[i]->isCreatedInSimulation()       ;
			//int isBackscatter                 = mcPList[i]->isBackscatter()               ; 
			//int isOverlay                     = mcPList[i]->isOverlay()                   ;
			//int vertexIsNotEndpointsOfParent  = mcPList[i]->vertexIsNotEndpointOfParent() ; 
			int nparents   = mcPList[i]->getParents().size();
			int pdgid      = mcPList[i]->getPDG();
			int id         = mcPList[i]->id();
			Int_t mother = 0, mid=0;
			if (nparents > 0) {
				MCParticle *Parent = mcPList[i]->getParents()[0];
				mother             = Parent->getPDG();
				mid                = Parent->id();
			}
			//if (    mcPList[i]->isCreatedInSimulation()        ) continue;
			//if (    mcPList[i]->isBackscatter()                ) continue;
			//if (    mcPList[i]->isOverlay()                    ) continue;
			//if (    mcPList[i]->vertexIsNotEndpointOfParent()  ) continue;
			if (mc->hasMothers( mcPList[i],111) ) continue;
			if (mc->hasMothers( mcPList[i],221) ) continue;
			/*
			printf("No. %3d, pdgid = %8d %8d, status = %2d %2d %2d %2d, mass=%9.4f\n", i, pdgid,mother,
					isCreatedInSimulation,      
					isBackscatter         ,     
					isOverlay              ,    
					vertexIsNotEndpointsOfParent,
					mcPList[i]->getMass());
			*/
			TLorentzVector p4p   = TLorentzVector(mcPList[i]->getMomentum(),mcPList[i]->getEnergy());
			mc_trkidx   [m_numParticle] = id ;
			mc_motheridx[m_numParticle] = mid ;
			mc_pdgid    [m_numParticle] = pdgid  ;
			mc_px       [m_numParticle] = p4p.X();
			mc_py       [m_numParticle] = p4p.Y();
			mc_pz       [m_numParticle] = p4p.Z();
			mc_pe       [m_numParticle] = p4p.T();
			m_numParticle++;
			if(m_numParticle>limit )
			{
				cout<<"m_numParticle >  : "<< limit << " " <<m_numParticle<<endl;
				break;
			}
		}
		//
		fillArray ("trkidx"    , "indexmc", (double*)mc_trkidx    , m_numParticle);
		fillArray ("motheridx" , "indexmc", (double*)mc_motheridx , m_numParticle);
		fillArray ("pdgid"     , "indexmc", (double*)mc_pdgid     , m_numParticle);
		fillArray ("mc_Px"     , "indexmc", (double*)mc_px        , m_numParticle);
		fillArray ("mc_Py"     , "indexmc", (double*)mc_py        , m_numParticle);
		fillArray ("mc_Pz"     , "indexmc", (double*)mc_pz        , m_numParticle);
		fillArray ("mc_En"     , "indexmc", (double*)mc_pe        , m_numParticle);
	}
}
