#include "PhotonCorrectionProcessor.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <utility>
#include <cmath>
#include <vector>
#include <EVENT/LCCollection.h>
#include <EVENT/MCParticle.h>
#include <IMPL/LCCollectionVec.h>

// ----- include for verbosity dependend logging ---------
#include <marlin/VerbosityLevels.h>

#include <TMath.h>
#include <TVector3.h>
#include <TLorentzVector.h>

using namespace lcio ;
using namespace marlin ;

PhotonCorrectionProcessor aPhotonCorrectionProcessor;

PhotonCorrectionProcessor::PhotonCorrectionProcessor()
	: Processor("PhotonCorrectionProcessor") {
		// Processor description
		_description = "Photon Correction Processor" ;

		// register steering parameters: name, description, class-variable, default value
		registerInputCollection( LCIO::RECONSTRUCTEDPARTICLE,
                                "InputCollection" ,
                                "Input collection of ReconstructedParticles",
                                _inputPFOsCollection,
                                std::string("PandoraPFOs"));

		registerInputCollection( LCIO::RECONSTRUCTEDPARTICLE,
                                "IsolationLeptonCollection" ,
                                "Input collection of ReconstructedParticles",
                                _inputIsolationLeptonCollection,
                                std::string("PandoraPFOs"));

		registerOutputCollection( LCIO::RECONSTRUCTEDPARTICLE,
                                "OutputCollectionWithoutRadiationPhoton",
                                "Copy of input collection but without FSR and Bremsstrahlung",
                                _outputPFOsRemovedIsoLepCollection,
                                std::string("PandoraPFOsWithoutRadiationPhoton") );

                registerOutputCollection( LCIO::RECONSTRUCTEDPARTICLE,
                                "OutputCollectionRecoverPhoton",
                                "Output collection of Recovered Photones",
                                _outputPhotonCollection,
                                std::string("RadiationPhoton") );

                registerProcessorParameter( "CosConeAngle",
                                "Cosine of the half-angle of the cone used in isolation criteria",
                                _cosConeAngle,
                                double(0.99));
}


void PhotonCorrectionProcessor::init() {
        streamlog_out(DEBUG) << "   init called  " << std::endl ;
        printParameters() ;
}

void PhotonCorrectionProcessor::processRunHeader( LCRunHeader* run) {
}

void PhotonCorrectionProcessor::processEvent( LCEvent * evt ) {
         _pfoCol = evt->getCollection( _inputPFOsCollection ) ;
         _IsolepCol = evt->getCollection(_inputIsolationLeptonCollection);
 
         LCCollectionVec* otPFOsRemovedIsoLepCol = new LCCollectionVec( LCIO::RECONSTRUCTEDPARTICLE ) ;
         otPFOsRemovedIsoLepCol->setSubset(true) ;

         LCCollectionVec* otIsoLepCol = new LCCollectionVec( LCIO::RECONSTRUCTEDPARTICLE );
         otIsoLepCol->setSubset(true);

         std::vector<TLorentzVector> v_isolep;
         std::vector<TLorentzVector> v_isolep_output;
         v_isolep.clear();
         //Isolep loop
         int nIsolep = _IsolepCol->getNumberOfElements();
         for(int l=0; l< nIsolep; l++){
               ReconstructedParticle* pfo = dynamic_cast<ReconstructedParticle*>( _IsolepCol->getElementAt(l) );
               TLorentzVector isolep_tmp;
               isolep_tmp.SetPxPyPzE(pfo->getMomentum()[0], pfo->getMomentum()[1],pfo->getMomentum()[2],pfo->getEnergy());
               v_isolep.push_back(isolep_tmp);
               v_isolep_output.push_back(isolep_tmp);
         }

         //PFO loop
         int npfo = _pfoCol->getNumberOfElements();
         for(int i = 0; i < npfo; i++){
               ReconstructedParticle* pfo = dynamic_cast<ReconstructedParticle*>( _pfoCol->getElementAt(i) );
               if(pfo->getType()!=22){
                    otPFOsRemovedIsoLepCol->addElement( pfo );
                    continue;
               }
               TLorentzVector photon_temp;
               photon_temp.SetPxPyPzE(pfo->getMomentum()[0], pfo->getMomentum()[1],pfo->getMomentum()[2],pfo->getEnergy());
               double angle_mini = -1;  int jmin = -1;
               for(int j=0; j< nIsolep; j++){
                    double angle = TMath::Cos(v_isolep.at(j).Angle(photon_temp.Vect()));
                    if(angle>angle_mini) {angle_mini = angle;jmin = j;}
               }
               if(angle_mini > _cosConeAngle){
                   v_isolep_output.at(jmin) += photon_temp;
                   ReconstructedParticle* iso_pfo = dynamic_cast<ReconstructedParticle*>( _IsolepCol->getElementAt(jmin) );
                   ReconstructedParticle *isopfo_tmp; 
                   //isopfo_tmp ->addParticle(iso_pfo);
//                   std::cout<<"Before recover , energy = "<<isopfo_tmp->getEnergy()<<std::endl;
//                   isopfo_tmp-> addParticle(pfo); 
//                   std::cout<<"After recover , energy = "<<iso_pfo->getEnergy()<<std::endl; 
                   otIsoLepCol->addElement( pfo );                      
               }else{
                   otPFOsRemovedIsoLepCol->addElement( pfo );
               }
         }

         evt->addCollection( otPFOsRemovedIsoLepCol, _outputPFOsRemovedIsoLepCollection.c_str() );
         evt->addCollection( otIsoLepCol, _outputPhotonCollection.c_str() );
}

void PhotonCorrectionProcessor::check( LCEvent * evt ) {
}

void PhotonCorrectionProcessor::end() {
}
